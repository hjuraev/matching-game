//
//  Networking.swift
//  Match Game
//
//  Created by Halimjon Juraev on 6/24/16.
//  Copyright © 2016 Halimjon Juraev. All rights reserved.
//

import UIKit

class ImageData {
    let farm: String
    let id: String
    let secret: String
    let server: String
    let title: String
    let url: String
    var image: UIImage?
    init(farm:String, id: String, secret: String, server: String, title: String){
        self.farm = farm
        self.id = id
        self.secret = secret
        self.server = server
        self.title = title
        
        self.url = "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_q.jpg"
    }
}

class Fetch: NSObject {
    
    let urlString = "https://api.flickr.com/services/rest/?method=flickr.photos.getRecent"
    let apiKey = "5423dbab63f23a62ca4a986e7cbb35e2"
    var perPage = String(NumberOfCards)
    var Data = [ImageData]()
    
    let session = NSURLSession.sharedSession()
    
    private func getPictureIDs(completion: (result: Bool) -> Void) {
        if let url = NSURL(string: "\(urlString)&api_key=\(apiKey)&per_page=\(perPage)&format=json&nojsoncallback=1&extras=tags") {
            session.dataTaskWithURL(url, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
                if error == nil {
                    if let responseData = data {
                        do {
                            let json = try NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.AllowFragments) as! [String: AnyObject]
                            if let photos = json["photos"]?["photo"] as? [AnyObject]{
                                for photo in photos {
                                    if photo["farm"] != nil || photo["id"] != nil || photo["secret"] != nil || photo["server"] != nil || photo["title"] != nil {
                                        self.Data.append(ImageData(farm: String(photo["farm"]!!), id: String(photo["id"]!!), secret: String(photo["secret"]!!), server: String(photo["server"]!!), title: String(photo["title"]!!)))
                                    }
                                    
                                }
                            }
                            completion(result: true)
                        } catch {
                            print("Cound not parse json")
                            completion(result: false)
                        }
                    }
                }
                completion(result: false)
            }).resume()
        }
    }
    
    
    func getImages(completion: (result: Bool, Views: [ViewClass]) -> Void){
        
        var View = [ViewClass](){
            didSet{
                if View.count == NumberOfCards {
                    completion(result: true, Views: View)
                }
            }
        }
        
        
        getPictureIDs { (result) in
            
            if result || self.Data.count == NumberOfCards {
                
                for x in self.Data {
                    self.session.dataTaskWithURL(NSURL(string:x.url)!, completionHandler: { data, response, error in
                        if error == nil {
                            if let imageData = data {
                                View.append(ViewClass(id: x.id, image: UIImage(data: imageData)!))
                            }
                        }
                    }).resume()
                }
            }
        }
    }
}