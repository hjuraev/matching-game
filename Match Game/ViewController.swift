//
//  ViewController.swift
//  Match Game
//
//  Created by Halimjon Juraev on 6/24/16.
//  Copyright © 2016 Halimjon Juraev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SelectedControl{
    
    @IBOutlet weak var Collection: UICollectionView!
    
    @IBOutlet weak var LoadingEffect: UIView!
    
    @IBOutlet weak var LoadingSpinner: UIActivityIndicatorView!
    
    var GameSize = NumberOfCards / 4
    let layout = UICollectionViewFlowLayout()
    var cardId: NSIndexPath?
    var gameScore: Int = 0 {
        didSet{
            
            
            ScoreLabel.text = "Your score: \(String(gameScore))"
            if gameScore == NumberOfCards * 2 {
                alert()
            }
        }
    }
    
    @IBOutlet weak var Control: ADVSegmentedControl!
    
    
    
    
    
    var Views = [ViewClass]() {
        didSet {
            Views.shuffleInPlace()
        }
    }
    @IBOutlet weak var ScoreLabel: UILabel!
    
    func newSelection() {
        LoadingSpinner.startAnimating()
        LoadingEffect.alpha = 0.4
        switch Control.selectedIndex {
        case 0:
            NumberOfCards = 8
            loadStuff()
            break
        case 1:
            NumberOfCards = 12
            loadStuff()
            break
        default:
            break
        }
        
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ScoreLabel.textColor = NaviBlue
        LoadingSpinner.startAnimating()
        LoadingSpinner.hidesWhenStopped = true
        LoadingEffect.backgroundColor = UIColor.darkGrayColor()
        loadStuff()
        Control.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        
        Collection.setCollectionViewLayout(layout, animated: false, completion: nil)
        Collection.delegate = self
        Collection.dataSource = self
    }
    
    func loadStuff(){
        
        let network = Fetch()
        network.getImages { (result, Views) in
            
            self.Views = Views
            self.Views.appendContentsOf(Views)
            
            dispatch_async(dispatch_get_main_queue()){
                self.Collection.reloadData()
                self.LoadingSpinner.stopAnimating()
                self.LoadingEffect.alpha = 0.0
                
                
            }
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if Views.count != 0 {
            return Views.count
        } else {
            return 0
        }
        
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let view = Views[indexPath.row]
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ViewCells", forIndexPath: indexPath) as? CustomCellView {
            cell.configureCell(view)
            return cell
        } else {
            return UICollectionViewCell()
        }
        
        
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? CustomCellView {
            
            if cell.state == .closed && self.cardId == nil {
                
                UIView.transitionWithView(cell.MainImage, duration: 1, options: .TransitionFlipFromLeft , animations: {
                    cell.MainImage.image = cell.kittenImage.image
                    cell.state = .flipped
                    self.cardId = indexPath
                    }, completion: { (result) in
                        
                })
                
            }
            
            
            if cell.state == .closed  && self.cardId != nil {
                if let previousCard = collectionView.cellForItemAtIndexPath(self.cardId!) as? CustomCellView {
                    cell.state = .flipped
                    self.cardId = nil
                    
                    
                    UIView.transitionWithView(cell.MainImage, duration: 1, options: .TransitionFlipFromLeft, animations: {
                        cell.MainImage.image = cell.kittenImage.image
                        
                        
                        if cell.id == previousCard.id {
                            cell.state = .matched
                            previousCard.state = .matched
                            self.gameScore += 2
                        }
                        
                        }, completion: { (result) in
                            
                            if cell.state == .flipped{
                                cell.state = .closed
                                UIView.transitionWithView(cell.MainImage, duration: 1, options: .TransitionFlipFromRight, animations: {
                                    cell.MainImage.image = cell.placholder
                                    }, completion: { (result) in
                                        
                                })
                                
                                if previousCard.state == .flipped {
                                    
                                    UIView.transitionWithView(previousCard.MainImage, duration: 1, options: .TransitionFlipFromRight, animations: {
                                        previousCard.MainImage.image = previousCard.placholder
                                        previousCard.state = .closed
                                        }, completion: { (result) in
                                            
                                    })
                                }
                            }
                            
                            
                    })
                    
                    
                    
                }
                
            }
            
            
        }
        
    }
    
    
    
    func rotated()
    {
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            print("landscape")
            Collection.reloadData()
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            print("Portrait")
            Collection.reloadData()
        }
        
    }
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        
        let height = (collectionView.frame.size.height / sqrt(CGFloat(NumberOfCards * 2))) - CGFloat(NumberOfCards)
        let width = (collectionView.frame.size.width / CGFloat(4))
        
        if height > width {
            
            return CGSize(width: width, height: width)
        } else {
            
            return CGSize(width: height, height: height)
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        
        return 5
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        
        return 0
    }
    
    
    
    func alert() {
        let alert = UIAlertController(title: "You WON", message: "Congratulations, you just got smarter", preferredStyle: .Alert)
        let actions = UIAlertAction(title: "Again", style: .Default) { (alert) in
            self.Views.shuffleInPlace()
            self.Collection.reloadData()
            self.gameScore = 0
        }
        alert.addAction(actions)
        self.showViewController(alert, sender: self)
        
    }
    
    
}

