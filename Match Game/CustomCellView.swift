//
//  CustomCellView.swift
//  Match Game
//
//  Created by Halimjon Juraev on 6/25/16.
//  Copyright © 2016 Halimjon Juraev. All rights reserved.
//

import UIKit

let SHADOW_COLOR: CGFloat = 157.0 / 255

class CustomCellView: UICollectionViewCell {
    
    @IBOutlet weak var MainImage: UIImageView!
    enum State {
        case flipped
        case matched
        case closed
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        layer.cornerRadius = 5.0
        layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).CGColor
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSizeMake(0.0, 2.0)
    }
    
    
    var state: State = .closed
    var kittenImage = UIImageView()
    let placholder = UIImage(named: "placeholder")
    var id: String!

    

    func configureCell(view: ViewClass) {
        id = view.id
        kittenImage.image = view.image
        MainImage.image = placholder
        state = .closed
        
    }
}
