//
//  ViewClass.swift
//  Match Game
//
//  Created by Halimjon Juraev on 6/24/16.
//  Copyright © 2016 Halimjon Juraev. All rights reserved.
//

import UIKit

class ViewClass: UIImageView {

    var id: String
    var flipped: Bool = false
    init(id: String, image: UIImage) {
        self.id = id
        super.init(image: image)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
